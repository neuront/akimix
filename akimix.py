PIANO = 0
SAX = 64

def mixer(tempo=0x80):
    if tempo > 0x7fff:
        raise ValueError('tempo out of range')

    SOUND_STOP = 8
    SOUND_START = 9
    TIMBRE_CHANGE = 0xc

    def write_int(out, value, length):
        byt = [0] * length
        i = 0
        while value != 0:
            byt[i] = value % 0x100
            value /= 0x100
            i += 1
        write_bytes(out, byt[::-1])

    def write_bytes(out, values):
        for b in values: out.write('%c' % b)

    def convert_seq_int(value):
        if value == 0:
            return [0]
        r = []
        while value != 0:
            r.append(value % 0x80)
            value /= 0x80
        for i in range(1, len(r)):
            r[i] |= 0x80
        return r[::-1]

    class Action:
        def __init__(self, action, time_seq_rep, args):
            self.action = action
            self.time = time_seq_rep
            self.args = args

    class Channel:
        def __init__(self, timbre, volume):
            self.volume = volume
            self.ticks = 0
            self.length = 4 # for 0x00ff2f00 4 bytes
            self.actions = []
            if timbre != 0:
                self.timbre(timbre)

        def timbre(self, new_timbre):
            self.actions.append(Action(TIMBRE_CHANGE, [0], (new_timbre,)))
            self.length += 3

        def note(self, pitch, ticks=0x80):
            self.actions.append(Action(SOUND_START, [0], (pitch, self.volume)))
            end = convert_seq_int(ticks)
            self.actions.append(Action(SOUND_STOP, end, (pitch, self.volume)))
            self.ticks += ticks
            self.length += (1 + 3 + len(end) + 3)
            return self

        def interval(self, ticks):
            self.ticks += ticks
            return self

        def write(self, channel_no, out):
            out.write('MTrk')
            write_int(out, self.length, 4)
            for a in self.actions:
                write_bytes(out, a.time)
                out.write('%c' % ((a.action << 4) | channel_no))
                write_bytes(out, a.args)

    class Mixer:
        def __init__(self, tempo):
            self.tempo = tempo
            self.channels = []

        def open_channel(self, timbre=PIANO, volume=0x40):
            if len(self.channels) == 16:
                raise BaseException('could only support up to 16 channels')
            channel = Channel(timbre, volume)
            self.channels.append(channel)
            return channel

        def write(self, filename):
            if len(self.channels) == 0:
                raise ValueError('no channels set')

            with open(filename, 'wb') as out:
                out.write('MThd')
                write_int(out, 6, 4)
                write_int(out, 1, 2)
                write_int(out, len(self.channels), 2)
                write_int(out, self.tempo, 2)
                for i, channel in enumerate(self.channels):
                    channel.write(i, out)
                    write_int(out, 0x00ff2f00, 4)

    return Mixer(tempo)

if __name__ == '__main__':
    m = mixer()
    # ch0 = m.open_channel(volume=0x7f, timbre=SAX)
    ch1 = m.open_channel(volume=0x7f).interval(0x20)
    for x in range(0x40, 0x70):
    #    ch0.note(x)
        ch1.note(x)
    m.write('out.mid')
